package
{
    import cocos2d.Cocos2DGame;
    import cocos2d.CCSprite;
    import cocos2d.ScaleMode;

    import UI.Label;

    import Loom.Animation.Tween;
    import Loom.Animation.EaseType;
    import Loom.Platform.Timer;

    public class MyGame extends Cocos2DGame
    {

        protected var timer:Timer;
        protected var score:Label;
        protected var scoreMessage:Label;
        protected var totalScore:Label;
        protected var total:Number;
        protected var scoreMessages:Vector.<String>;
        protected var winBtn:CCSprite;
        

        override public function run():void
        {
            // Comment out this line to turn off automatic scaling.
            layer.scaleMode = ScaleMode.LETTERBOX;

            super.run();

            // Setup anything else, like UI, or game objects.
            var bg = CCSprite.createFromFile("assets/bg.png")
            bg.x = 240;
            bg.y = 160;
            bg.scale = 0.5;
            layer.addChild(bg);
            
            winBtn = CCSprite.createFromFile("assets/winBtn.png");
            winBtn.x = layer.designWidth/2;
            winBtn.y = layer.designHeight/2;
            winBtn.scale = 0.66;
            layer.addChild(winBtn);
            winBtn.onTouchBegan = function() {
                win();
            }

            var totalText = new Label("assets/Curse-hd.fnt");
            totalText.text = "Total:";
            totalText.x = 40;
            totalText.y = 320;
            totalText.scale = 0.5;
            layer.addChild(totalText);

            total = 0;
            totalScore = new Label("assets/Curse-hd.fnt");
            totalScore.text = "0";
            totalScore.x = 130;
            totalScore.y = 320;
            totalScore.scale = 0.5;
            layer.addChild(totalScore);

            createScoreLabels();
        }

        protected function createScoreLabels()
        {
            score = new Label("assets/Curse-hd.fnt");
            score.text = "+100";
            score.x = layer.designWidth/2;
            score.scale = 0.5;
            score.y = 600;
            layer.addChild(score);

            scoreMessage = new Label("assets/Curse-hd.fnt");
            scoreMessage.x = layer.designWidth/2;
            scoreMessage.y = -600;
            scoreMessage.scale = 0.5;
            layer.addChild(scoreMessage);

            scoreMessages = ["You're Awesome!", "You're Crazy Good!", "You're On Fire!",
                "You're The Best", "You Win!", "You Can't Be Beat!", "OH YEAH!", "God Like!",
                "Unstoppable!", "U Da Bomb!", "Go You!"];
        }

        protected function win() 
        {
            if(!Tween.isTweening(score) && !Tween.isTweening(scoreMessage) && !Tween.isTweening(winBtn)) {
                score.y = 275;
                total += 100;
                totalScore.text = total.toString();

                scoreMessage.text = getScoreMessage(); 
                scoreMessage.x = layer.designWidth/2;
                scoreMessage.y = 80;
                
                
                Tween.to(score, 0.3, {"scale": 0.5, "ease": EaseType.EASE_OUT_BACK});
                Tween.to(score, 0.3, {"y": 500, "ease": EaseType.EASE_IN_BACK, "delay": 0.3});

                Tween.to(scoreMessage, 0.5, {"scale": .95, "ease": EaseType.EASE_OUT_BACK});
                Tween.to(scoreMessage, 0.5, {"y": -200, "ease": EaseType.EASE_IN_BACK, "delay": 0.3});

                Tween.to(winBtn, 0.1, {"y": ((layer.designHeight/2)-1), "ease": EaseType.EASE_IN_BACK});
                Tween.to(winBtn, 0.1, {"y": (layer.designHeight/2), "ease": EaseType.EASE_IN_BACK, "delay": 0.1});
            }

        }

        protected function getScoreMessage():String
        {
            scoreMessage.scale = 1;
            switch(total) {
                case 900:
                    return "PLEASE STOP!";
                case 1600:
                    return "OH GOD!";
                case 2200:
                    return "HELP!";
                case 3400:
                    return "PLEASE, PLEASE STOP!";
                case 4200:
                    return "YOU MONSTER!";
                case 4300:
                    return "THINK OF THE CHILDREN!";
                case 6000:
                    return "You're The Man Now Dawg";
                case 6100: 
                    return "I HAD A DOG ONCE";
                case 6200: 
                    return "SERIOUSLY, HIS NAME WAS SPOT";
                case 6300:
                    return "HE WAS SMASHED WHEN YOU CLIKED THE BUTTON";
                case 6400:
                    return "That's Why I Asked You To Stop";
                default:
                    scoreMessage.scale = 0.5;
                    var index = Math.floor(Math.random()*scoreMessages.length);
                    
                    return scoreMessages[index].toString();
            }

            return "You're Awesome";
        }
    }
}